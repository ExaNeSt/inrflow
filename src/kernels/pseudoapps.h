#ifndef _pseudoapps
#define _pseudoapps

void gups(application *app);

void nbodies(application *app);

void shift(application *app);

void bisection(application *app);

void hotregion(application *app);

void mapreduce(application *app);

void ptp(application *app);

void randomapp(application *app);

void randomappdcn(application *app);

void many_all2all(application *app);

void many_all2all_rnd(application *app);

void shift_incremental(application *app);

void shift_random(application *app);

#endif
