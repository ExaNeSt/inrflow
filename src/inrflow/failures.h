#ifndef _failures
#define _failures

void set_failures();

long check_failures();

bool_t are_there_failures();

#endif
