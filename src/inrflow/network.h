#ifndef _network
#define _network

void check_network_consistency();

void construct_network_electric();

void construct_network_photonic();

void release_network_electric();

void release_network_photonic();

void reset_network();
#endif
